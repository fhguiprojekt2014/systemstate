﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SystemState.Service
{
    class ServerStateService : IStateService
    {
        private Uri uri;
        HttpClient client;
        State state;

        public ServerStateService(string url)
        {
            this.uri = new Uri(url);
            this.client = new HttpClient();

            Storage hda1 = new Storage();
            hda1.name = "/";
            hda1.capacity = 0;
            hda1.usedCapacity = 0;


            State s = new State();
            s.storages = new Storage[] { hda1 };
            s.cpuLoad = 0;
            s.cpuTemperature = 0;
            s.hdWorkLoad = 0;
            s.cpuLoadTemporary = 0;
            s.countProcess = 0;
            s.ramLoad = 0;
            s.ramUsed = 0;

            this.state = s;
        }

        public State getState()
        {
            return state;
        }


        public async void refreshState()
        {
            string response = await this.client.GetStringAsync(this.uri);

            state = JsonConvert.DeserializeObject<State>(response);
        }
    }
}
