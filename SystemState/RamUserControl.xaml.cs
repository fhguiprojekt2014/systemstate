﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WinRTXamlToolkit.Controls.DataVisualization.Charting;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace SystemState
{
    public sealed partial class RamUserControl : UserControl
    {

        public List<SystemState.MainPage.TimeStateItem> States;

        private State lastState;
        private LineSeries ls;



        private String ramLoad
        {
            get { return (String)GetValue(ramLoadProperty); }
            set { SetValue(ramLoadProperty, value + " %"); }
        }

        public static readonly DependencyProperty ramLoadProperty =
        DependencyProperty.Register("ramLoad",
            typeof(string),
            typeof(RamUserControl),
            new PropertyMetadata("0%")
        );

        private String ramGesamt
        {
            get { return (String)GetValue(ramGesamtProperty); }
            set { SetValue(ramGesamtProperty, "RAM Gesamt: " + value + " MB"); }
        }

        public static readonly DependencyProperty ramGesamtProperty =
        DependencyProperty.Register("ramGesamt",
            typeof(string),
            typeof(RamUserControl),
            new PropertyMetadata("0MB")
        );
        private String ramUsed
        {
            get { return (String)GetValue(ramUsedProperty); }
            set { SetValue(ramUsedProperty, "in Verwendung: " + value + " MB"); }
        }

        public static readonly DependencyProperty ramUsedProperty =
        DependencyProperty.Register("ramUsed",
            typeof(string),
            typeof(RamUserControl),
            new PropertyMetadata("0 MB")
        );


        public RamUserControl()
        {
            this.InitializeComponent();
        }

        public void createChart()
        {
            ls = new LineSeries();

            ls.ItemsSource = States;
            ls.IndependentValuePath = "Time";
            ls.Title = "";
            ls.DependentValuePath = "State.ramLoad";

            ls.DependentRangeAxis = new LinearAxis() { Minimum = 0, Maximum = 100, Orientation = AxisOrientation.Y };

            Chart.Series.Add(ls);

        }

        public void updateChart(SystemState.MainPage.ServiceChartItem sc)
        {
            lastState = States[States.Count - 1].State;

            if (lastState == null)
            {
                return;
            }

            ramLoad = lastState.ramLoad.ToString();
            ramUsed = lastState.ramUsedCapacity.ToString();
            ramGesamt = lastState.ramCapacity.ToString();

            // If we switch the Service, we need the new Items
            ((LineSeries)Chart.Series[0]).ItemsSource = States;
            ((LineSeries)Chart.Series[0]).Refresh();
        }
    }
}
