﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WinRTXamlToolkit.Controls.DataVisualization.Charting;


// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace SystemState
{
    public sealed partial class HDUserControl : UserControl
    {

        public List<SystemState.MainPage.TimeStateItem> States;

        private State lastState;
        private LineSeries ls;



        private String hdWorkLoad
        {
            get { return (String)GetValue(hdWorkLoadProperty); }
            set { SetValue(hdWorkLoadProperty, value + " Kb/s"); }
        }

        public static readonly DependencyProperty hdWorkLoadProperty =
        DependencyProperty.Register("hdWorkLoad",
            typeof(string),
            typeof(HDUserControl),
            new PropertyMetadata("0 Kb/s ")
        );

        private String midHdWorkLoad
        {
            get { return (String)GetValue(midHdWorkLoadProperty); }
            set { SetValue(midHdWorkLoadProperty, "mittlere Auslastung: " + value + " %"); }
        }

        public static readonly DependencyProperty midHdWorkLoadProperty =
        DependencyProperty.Register("midHdWorkLoad",
            typeof(string),
            typeof(HDUserControl),
            new PropertyMetadata("0%")
        );

        private String hdCapacity
        {
            get { return (String)GetValue(hdCapacityProperty); }
            set { SetValue(hdCapacityProperty, "Freie Kapazität: " + value + " GB"); }
        }

        public static readonly DependencyProperty hdCapacityProperty =
        DependencyProperty.Register("hdCapacity",
            typeof(string),
            typeof(HDUserControl),
            new PropertyMetadata("0GB")
        );



        public HDUserControl()
        {
            this.InitializeComponent();
        }

        public void createChart()
        {
            ls = new LineSeries();

            ls.ItemsSource = States;
            ls.IndependentValuePath = "Time";
            ls.Title = "";
            ls.DependentValuePath = "State.hdWorkLoad";
            ls.DependentRangeAxis = new LinearAxis() { Minimum = 0, Orientation = AxisOrientation.Y };

            Chart.Series.Add(ls);
        }

        public void updateChart(SystemState.MainPage.ServiceChartItem sc)
        {

            lastState = States[States.Count - 1].State;

            if (lastState == null)
            {
                return;
            }

            int i = 0;
            int sum = 0;

            foreach (SystemState.MainPage.TimeStateItem ts in States)
            {
                if (ts.State != null)
                {
                    i++;
                    sum += ts.State.hdWorkLoad;
                }
            }

            midHdWorkLoad = (sum / i).ToString();

            hdCapacity = (lastState.storages[0].capacity - lastState.storages[0].usedCapacity).ToString();

            if (lastState != null)
            {
                hdWorkLoad = lastState.hdWorkLoad.ToString();
            }

            // If we switch the Service, we need the new Items
            ((LineSeries)Chart.Series[0]).ItemsSource = States;
            ((LineSeries)Chart.Series[0]).Refresh();
        }
    }
}
