﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemState.Service
{
    public interface IStateService
    {
        State getState();

        void refreshState();
    }
}
