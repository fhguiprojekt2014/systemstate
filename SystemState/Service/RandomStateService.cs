﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemState.Service
{
    class RandomStateService : IStateService
    {

        public Random random;

        public RandomStateService()
        {
            random = new Random();
        }

        public State getState()
        {
            int randomNumber = random.Next(1, 19);

            Storage hda1 = new Storage();
            hda1.name = "/";
            // 20GB
            hda1.capacity = (UInt64)26;
            hda1.usedCapacity = (UInt64)randomNumber ;


            State s = new State();
            s.storages = new Storage[] { hda1 };
            s.cpuLoad = random.Next(5, 60);
            s.cpuTemperature = random.Next(45, 85);
            s.hdWorkLoad = random.Next(0, 100);
            s.countProcess = (uint)random.Next(53, 78);
            s.ramCapacity = 4092;
            s.ramLoad = random.Next(55, 95);
            s.ramUsedCapacity = (uint) random.Next(3110, 3600);

            return s;
        }

        public async void refreshState()
        {
            return;
        }
    }
}
