﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemState
{
    public class Storage
    {

       
        /// <summary>
        /// Returns the name of the Storage like
        /// </summary>
        public string name;

        /// <summary>
        /// The capacity of the Storage
        /// </summary>
        public UInt64 capacity;

        /// <summary>
        /// The used capacity of the Storage
        /// </summary>
        public UInt64 usedCapacity;

        
    }
}
