﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemState
{
    public class State
    {
        /// <summary>
        /// Array of Load Values. Each for a CPU Core.
        /// 1 means 100%
        /// </summary>
        public int cpuLoad{get; set;}

        /// <summary>
        /// Cpu Load for Temporary using in preVersion of SystemState
        /// </summary>
        public int cpuLoadTemporary { get; set; }

        /// <summary>
        /// RAM Load for using 
        /// </summary>
        public int ramLoad { get; set; }

        /// <summary>
        /// RAM Used
        /// </summary>
        public int ramUsed { get; set; }

        /// <summary>
        /// Temperatur of the cpu
        /// </summary>
        public int cpuTemperature { get; set; }

        /// <summary>
        /// Workload of HDD usage
        /// </summary>
        public int hdWorkLoad { get; set; }

        /// <summary>
        /// Returns a array of IStorage.
        /// <seealso cref="SystemState.Storage"/>
        /// </summary>
        public Storage[] storages;

        /// <summary>
        /// The Capacity of the main Storage in MByte
        /// </summary>
        public uint ramCapacity;
        
        /// <summary>
        /// The used capacity of the main Storage in MByte
        /// </summary>
        public uint ramUsedCapacity;

        /// <summary>
        /// The used capacity of the main Storage in MByte
        /// </summary>
        public uint countProcess;





        public int ramLoadTemporary { get; set; }
    }
}
