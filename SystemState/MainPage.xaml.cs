﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WinRTXamlToolkit.Controls.DataVisualization.Charting;
using SystemState.Service;
using Windows.UI.Core;
using Windows.UI.Popups;
using SystemState;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace SystemState
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet werden kann oder auf die innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        // private IStateService stateService = new ServerStateService("http://localhost:8080");
        // private IStateService stateService = new RandomStateService();

        // private List<TimeStateItem> chartItems = new List<TimeStateItem>();

        private List<ServiceChartItem> serviceChartList = new List<ServiceChartItem>();

        DispatcherTimer stateUpdateTimer;

        DispatcherTimer serviceSwitchUpdateTimer;

        private ServiceChartItem currentService;

        private int currentServiceId = 0;

        private int waitRounds = 0;

        private String serverName
        {
            get { return (String)GetValue(serverNameProperty); }
            set { SetValue(serverNameProperty, value); }
        }

        public static readonly DependencyProperty serverNameProperty = DependencyProperty.Register("serverName",
            typeof(string),
            typeof(MainPage),
            new PropertyMetadata("serverName")
        );

        public MainPage()
        {
            this.InitializeComponent();

            ServiceChartItem sc = new ServiceChartItem();
            sc.Name = "Random Data Service";
            sc.TimeStateList = new List<TimeStateItem>();
            //sc.StateService = new ServerStateService("http://state.o11.eu/");
            sc.StateService = new RandomStateService();
            sc.button = default_Tab;
            sc.button.Click += new RoutedEventHandler(this.Tab_Click);


            this.serverName = sc.Name;
            this.InitializeChartItems(sc.TimeStateList);

            TempControler.States = sc.TimeStateList;
            TempControler.createChart();
            HdControler.States = sc.TimeStateList;
            HdControler.createChart();
            CpuControler.States = sc.TimeStateList;
            CpuControler.createChart();
            RamControler.States = sc.TimeStateList;
            RamControler.createChart();

            serviceChartList.Add(sc);
            currentService = sc;



            this.stateUpdateTimer = new DispatcherTimer();
            this.stateUpdateTimer.Interval = TimeSpan.FromSeconds(2);
            this.stateUpdateTimer.Tick += this.update;
            this.stateUpdateTimer.Start();

            this.serviceSwitchUpdateTimer = new DispatcherTimer();
            this.serviceSwitchUpdateTimer.Interval = TimeSpan.FromSeconds(5);
            this.serviceSwitchUpdateTimer.Tick += this.clickNextButton;
            this.serviceSwitchUpdateTimer.Start();
            
            // Make immediately the first update
            update(null, null);
           
        }

        private void InitializeChartItems(List<TimeStateItem> chartItems)
        {
            chartItems.Add(new TimeStateItem()
            {
                Time = "60",
                State = null
            });
            chartItems.Add(new TimeStateItem()
            {
                Time = "55",
                State = null
            });
            chartItems.Add(new TimeStateItem()
            {
                Time = "50",
                State = null
            });
            chartItems.Add(new TimeStateItem()
            {
                Time = "45",
                State = null
            });
            chartItems.Add(new TimeStateItem()
            {
                Time = "40",
                State = null
            });
            chartItems.Add(new TimeStateItem()
            {
                Time = "35",
                State = null
            });
            chartItems.Add(new TimeStateItem()
            {
                Time = "30",
                State = null
            });
            chartItems.Add(new TimeStateItem()
            {
                Time = "25",
                State = null
            });
            chartItems.Add(new TimeStateItem()
            {
                Time = "20",
                State = null
            });
            chartItems.Add(new TimeStateItem()
            {
                Time = "15",
                State = null
            });
            chartItems.Add(new TimeStateItem()
            {
                Time = "10",
                State = null
            });
            chartItems.Add(new TimeStateItem()
            {
                Time = "5",
                State = null
            });
            chartItems.Add(new TimeStateItem()
            {
                Time = "0",
                State = null
            });
        }

        private void update(object sender, object e)
        {
            //Alle States Refreshen
            for(int i = 0; i < serviceChartList.Count; i++)
            {
                serviceChartList[i].StateService.refreshState();

                //und die Daten von jedem um eins verschieben
                var ChartItem = serviceChartList[i].TimeStateList;

                for(int j = 0; j < ChartItem.Count; j++)
                {
                    if(j < ChartItem.Count - 1)
                    {
                        // Verschieben
                        ChartItem[j].State = ChartItem[j + 1].State;
                    }
                    else
                    {
                        // Neuen Zustand holen
                        ChartItem[j].State = serviceChartList[i].StateService.getState();
                    }
                }
            }

            this.updateCharts();
            currentService.FirstRun = false;
        }

        private void updateCharts()
        {
            TempControler.updateChart(currentService);
            HdControler.updateChart(currentService);
            CpuControler.updateChart(currentService);
            RamControler.updateChart(currentService);
        }

        private void switchService()
        {
            TempControler.States = currentService.TimeStateList;
            HdControler.States = currentService.TimeStateList;
            CpuControler.States = currentService.TimeStateList;
            RamControler.States = currentService.TimeStateList;

            this.serverName = currentService.Name;
            this.updateCharts();
        }

        private void Beenden_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Application.Current.Exit();
        }


        public DependencyProperty On
        {
            get;
            set;
        }


        private void ThemeToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            this.RequestedTheme = ElementTheme.Dark;
        }


        private void ThemeToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            this.RequestedTheme = ElementTheme.Light;
        }


        private void Random_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (random_name.Text == "")
            {
                random_name.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Red);
                return;
            }
            else
            {
                random_name.BorderBrush = new SolidColorBrush(Windows.UI.Colors.DarkGray);
            }
        }


        private void RandomHizufuegen_Tapped(object sender, TappedRoutedEventArgs e)
        {

            ServiceChartItem sc = new ServiceChartItem();

            Button Tab = new Button();
            Tab.Content = random_name.Text;
            Tab.Tag = random_name.Text;
            Tab.Width = 138;
            Tab.Height = 38;
            Tab.Click += new RoutedEventHandler(this.Tab_Click);

            sc.Name = random_name.Text;
            sc.StateService = new RandomStateService();
            sc.TimeStateList = new List<TimeStateItem>();
            this.InitializeChartItems(sc.TimeStateList);
            sc.button = Tab;

            this.serviceChartList.Add(sc);
            AppBarContentPanel.Children.Insert(0, Tab);
            AddServerButton.Flyout.Hide();
        }


        private void Netzwerk_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (url.Text == "")
            {
                url.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Red);
            }
            else
            {
                url.BorderBrush = new SolidColorBrush(Windows.UI.Colors.DarkGray);
            }

            if (server_name.Text == "")
            {
                server_name.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Red);
            }
            else
            {
                server_name.BorderBrush = new SolidColorBrush(Windows.UI.Colors.DarkGray);
            }

            if (url.Text == "" || server_name.Text == "")
            {
                return; 
            }

            ServiceChartItem sc = new ServiceChartItem();
            sc.Name = server_name.Text;
            sc.StateService = new ServerStateService(url.Text);
            sc.TimeStateList = new List<TimeStateItem>();
            this.InitializeChartItems(sc.TimeStateList);

            Button Tab = new Button();
            Tab.Content = server_name.Text;
            Tab.Tag = server_name.Text;
            Tab.Width = 138;
            Tab.Height = 38;
            Tab.Click += new RoutedEventHandler(this.Tab_Click);

            sc.button = Tab;

            this.serviceChartList.Add(sc);
            AppBarContentPanel.Children.Insert(0, Tab);
            AddServerButton.Flyout.Hide();
        }

        private void Tab_Click(object sender, RoutedEventArgs e)
        {
            // On manualy click, wait 2 round
            this.waitRounds = 2;
            this.markAllButtonNotActive();

            Button btn = sender as Button;

            btn.Background = new SolidColorBrush(Windows.UI.Colors.DarkGray);
            ServiceChartItem sc = this.serviceChartList.Find(x => x.button.Equals(btn));

            currentService = sc;

            this.switchService();
            //this.BottomAppBar.IsOpen = false;
            //this.TopAppBar.IsOpen = false;
        }

        private void markAllButtonNotActive()
        {
            for(int i = 0; i < this.serviceChartList.Count; i++)
            {
                this.serviceChartList[i].button.Background = new SolidColorBrush(Windows.UI.Colors.LightGray);
            }
        }

        private void clickNextButton(object sender, object e)
        {
            if (this.waitRounds-- > 0)
            {
                return;
            }

            this.currentServiceId++;
            if (this.currentServiceId >= this.serviceChartList.Count)
            {
                this.currentServiceId = 0;
            }

            ServiceChartItem sc = this.serviceChartList[this.currentServiceId];
            this.Tab_Click(sc.button, null);

            // overwrite waitRounds, because its not manual
            this.waitRounds = 0;
        }

        public class TimeStateItem
        {
            private string _time;

            public string Time
            {
                get
                {
                    return _time;
                }
                set
                {
                    _time = value;
                }
            }
            private State _state;

            public State State
            {
                get
                {
                    return _state;
                }
                set
                {
                    _state = value;
                }
            }
            public TimeStateItem()
            {
            }
        }

        public class ServiceChartItem
        {
            private string _name;

            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                     _name = value;
                     
                }
            }

            public Button button;
            
            public bool FirstRun = true;

            private IStateService _stateService;

            public IStateService StateService
            {
                get
                {
                    return _stateService;
                }
                set
                {
                    _stateService = value;
                }
            }


            private List<TimeStateItem> _timeStateList;

            public List<TimeStateItem> TimeStateList
            {
                get
                {
                    return _timeStateList;
                }
                set
                {
                    _timeStateList = value;
                }
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


        private void ComboBox_2Sek(object sender, TappedRoutedEventArgs e)
        {

            this.serviceSwitchUpdateTimer.Interval = TimeSpan.FromSeconds(2);
        }

        private void ComboBox_5Sek(object sender, TappedRoutedEventArgs e)
        {

            this.serviceSwitchUpdateTimer.Interval = TimeSpan.FromSeconds(5);
        }

        private void ComboBox_10Sek(object sender, TappedRoutedEventArgs e)
        {

            this.serviceSwitchUpdateTimer.Interval = TimeSpan.FromSeconds(10);
        }

        private void TempChange50degree_Tapped(object sender, TappedRoutedEventArgs e)
        {
            TempControler.tempLimit = 50;
        }

        private void TempChange60degree_Tapped(object sender, TappedRoutedEventArgs e)
        {
            TempControler.tempLimit = 60;
        }

        private void TempChange70degree_Tapped(object sender, TappedRoutedEventArgs e)
        {
            TempControler.tempLimit = 70;
        }
    }
}