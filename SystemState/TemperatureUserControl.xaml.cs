﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WinRTXamlToolkit.Controls.DataVisualization.Charting;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace SystemState
{
    public sealed partial class TemperatureUserControl : UserControl
    {
        public List<SystemState.MainPage.TimeStateItem> States;

        private State lastState;

        private LineSeries ls;

        public int tempLimit { get; set; }

        private String CPUTemperatur
        {
            get { return (String)GetValue(CPUTemperaturProperty); }
            set { SetValue(CPUTemperaturProperty, value + "°"); }
        }

        public static readonly DependencyProperty CPUTemperaturProperty =
            DependencyProperty.Register("CPUTemperatur",
                typeof(string),
                typeof(TemperatureUserControl),
                new PropertyMetadata("0°")
            );


        private int MaxTemperatur
        {
            get { return Convert.ToInt32(GetValue(MaxTemperaturProperty)); }
            set { SetValue(MaxTemperaturProperty, value); }
        }

        public static readonly DependencyProperty MaxTemperaturProperty =
            DependencyProperty.Register("MaxTemperatur",
                typeof(int),
                typeof(TemperatureUserControl),
                new PropertyMetadata("0")
            );

        private int MinTemperatur
        {
            get { return Convert.ToInt32(GetValue(MinTemperaturProperty)); }
            set { SetValue(MinTemperaturProperty, value); }
        }

        public static readonly DependencyProperty MinTemperaturProperty =
            DependencyProperty.Register("MinTemperatur",
                typeof(int),
                typeof(TemperatureUserControl),
                new PropertyMetadata("0")
            );

        public TemperatureUserControl()
        {
            this.tempLimit = 60;
            this.InitializeComponent();
        }

        public void createChart()
        {
            ls = new LineSeries();

            ls.ItemsSource = States;
            ls.Title = "";
            ls.IndependentValuePath = "Time";
            ls.DependentValuePath = "State.cpuTemperature";

            ls.DependentRangeAxis = new LinearAxis() { Minimum = 0, Maximum = 100, Orientation = AxisOrientation.Y };

            Chart.Series.Add(ls);
        }

        public void updateChart(SystemState.MainPage.ServiceChartItem sc)
        {
            lastState = States[States.Count - 1].State;

            if (lastState == null)
            {
                return;
            }

            if (sc.FirstRun)
            {
                MaxTemperatur = lastState.cpuTemperature;
                MinTemperatur = lastState.cpuTemperature;
            }

            if (MaxTemperatur < lastState.cpuTemperature)
            {
                MaxTemperatur = lastState.cpuTemperature;
            }
            else if (MinTemperatur > lastState.cpuTemperature)
            {
                MinTemperatur = lastState.cpuTemperature;
            }

            if (lastState.cpuTemperature > this.tempLimit)
            {
                temperatur_grid.Background = new SolidColorBrush(Windows.UI.Colors.Red);
            }
            else
                temperatur_grid.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0xA6, 0xA6, 0xA6));

            
            CPUTemperatur = lastState.cpuTemperature.ToString();

            // If we switch the Service, we need the new Items
            ((LineSeries)Chart.Series[0]).ItemsSource = States;
            ((LineSeries)Chart.Series[0]).Refresh();
        }
    }
}
