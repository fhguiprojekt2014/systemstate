﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WinRTXamlToolkit.Controls.DataVisualization.Charting;


// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace SystemState
{
    public sealed partial class CPUUserControl : UserControl
    {
        public List<SystemState.MainPage.TimeStateItem> States;

        private State lastState;

        private LineSeries ls;

        private String cpuLoadValue
        {
            get { return (String)GetValue(cpuLoadProperty); }
            set { SetValue(cpuLoadProperty, value + " %"); }
        }

        public static readonly DependencyProperty cpuLoadProperty = DependencyProperty.Register("cpuLoad",
            typeof(string),
            typeof(CPUUserControl),
            new PropertyMetadata("0%")
        );

        private String midCpuLoad
        {
            get { return (String)GetValue(midCpuLoadProperty); }
            set { SetValue(midCpuLoadProperty, "mittlere Auslastung: " + value + " %"); }
        }

        public static readonly DependencyProperty midCpuLoadProperty =
        DependencyProperty.Register("midCpuLoad",
            typeof(string),
            typeof(CPUUserControl),
            new PropertyMetadata("0%")
        );

        private String countProcess
        {
            get { return (String)GetValue(countProcessProperty); }
            set { SetValue(countProcessProperty, "Anzahl Prozesse:      " + value); }
        }

        public static readonly DependencyProperty countProcessProperty =
        DependencyProperty.Register("countProcess",
            typeof(string),
            typeof(CPUUserControl),
            new PropertyMetadata("Anzahl Prozesse: 0")
        );

        public CPUUserControl()
        {
            this.InitializeComponent();
        }

        public void createChart()
        {
            ls = new LineSeries();

            ls.ItemsSource = States;

            ls.IndependentValuePath = "Time";
            ls.Title = "";
            ls.DependentValuePath = "State.cpuLoad";

            ls.DependentRangeAxis = new LinearAxis() { Minimum = 0, Maximum = 100, Orientation = AxisOrientation.Y };

            Chart.Series.Add(ls);

        }

        public void updateChart(SystemState.MainPage.ServiceChartItem sc)
        {

            lastState = States[States.Count - 1].State;

            if (lastState == null)
            {
                return;
            }

            int i = 0;
            int sum = 0;

            foreach (SystemState.MainPage.TimeStateItem ts in States)
            {
                if (ts.State != null)
                {
                    i++;
                    sum += ts.State.cpuLoad;
                }
            }

            midCpuLoad = (sum / i).ToString();


            cpuLoadValue = lastState.cpuLoad.ToString();
            countProcess = lastState.countProcess.ToString();


            // If we switch the Service, we need the new Items
            ((LineSeries)Chart.Series[0]).ItemsSource = States;
            ((LineSeries)Chart.Series[0]).Refresh();
        }
    }
}
